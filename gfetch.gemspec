Gem::Specification.new do |s|
  s.name        = 'gfetch'
  s.version     = '0.0.2'
  s.date        = '2019-10-16'
  s.summary     = "Very naive gmail fetcher"
  s.description = "Downloads your Gmail inbox into a maildir"
  s.authors     = ["Paul Roberts"]
  s.email       = 'pmr@stelo.org.uk'
  s.files       = [
    "lib/gfetch/local.rb",
    "lib/gfetch/remote.rb",
    "lib/gfetch/config.rb",
    "lib/gfetch.rb",
  ]
  s.license       = 'GPL-3.0-or-later'
  s.executables << 'gfetch'
end
