require "maildir"

class Local
  def initialize(cfg)
    if cfg.verbose
      puts "Writing to maildir: #{cfg.maildir}"
    end
    @maildir = Maildir.new cfg.maildir
  end

  def put_message(msg)
    @maildir.add msg
  end
end
