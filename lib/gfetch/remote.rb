require "google/apis/gmail_v1"
require "googleauth"
require "googleauth/stores/file_token_store"
require "fileutils"

class MessagesResult
  attr_reader :count

  def initialize(res, rem)
    @count = res.result_size_estimate
    @rem = rem
    @res = res
  end

  def each(&block)
    while not @res.messages.nil? do
      @res.messages.each do |msg_ptr|
        msg = @rem.get_message(msg_ptr.id)
        block.call(msg.raw)
      end
      @rem.post_process @res
      if @res.next_page_token.nil? then break end
      @res = @rem.next_messages(@res.next_page_token)
    end
  rescue StandardError => e
    @rem.log.error(e)
    @rem.log.error("error, aborting fetch")
  end

end

class Remote
  OOB_URI = "urn:ietf:wg:oauth:2.0:oob".freeze
  SCOPE = Google::Apis::GmailV1::AUTH_GMAIL_MODIFY

  ##
  # Ensure valid credentials, either by restoring from the saved credentials
  # files or intitiating an OAuth2 authorization. If authorization is required,
  # the user's default browser will be launched to approve the request.
  #
  # @return [Google::Auth::UserRefreshCredentials] OAuth2 credentials
  def authorize
    client_id = Google::Auth::ClientId.from_file @cfg.cred_file
    token_store = Google::Auth::Stores::FileTokenStore.new file: @cfg.token_file
    authorizer = Google::Auth::UserAuthorizer.new client_id, SCOPE, token_store
    user_id = "default"
    credentials = authorizer.get_credentials user_id
    if credentials.nil?
      url = authorizer.get_authorization_url base_url: OOB_URI
      puts "Opening the following URL in the browser " + url
      system "#{@cfg.browser_command} '#{url}'"
      code = gets
      credentials = authorizer.get_and_store_credentials_from_code(
        user_id: user_id, code: code, base_url: OOB_URI
      )
    end
    credentials
  end

  def initialize(cfg)
    @cfg = cfg
    @creds = authorize
    @api = Google::Apis::GmailV1::GmailService.new
    @api.client_options.application_name = "gmail_fetcher"
    @api.authorization = @creds
    @user = cfg.remote_user

    @add_labels = @cfg.add_labels.map do |l|
      find_label(l) or raise "bad label #{l}"
    end
    log.info("found labels: #{@add_labels.join(",")}")
  end

  def get_message(id)
    m = @api.get_user_message @user, id, format: "raw"
    log.info("Fetched message #{m.id} (length: #{m.size_estimate})")
    m
  end

  def get_messages

    res = next_messages

    MessagesResult.new(res, self)
  end

  def find_label(name)
    @labels ||= @api.list_user_labels(@user).labels
    @labels.find { |l| l.name == name }.id
  end

  # TODO: this should be customisable, e.g
  #     post_process:
  #         - archive
  #         - addtag:FETCHED
  #
  #  OR:
  #     post_process:
  #         archive: true
  #         add_labels:
  #            - FETCHED
  #            - OLD
  #         remove_labels:
  #            - ... etc
  def post_process(res)
    msgs = res.messages
    ids = msgs.map { |m| m.id }

    log.info("post processing #{ids.length} messages: #{ids.join(",")}")

    req = Google::Apis::GmailV1::BatchModifyMessagesRequest
            .new ids: ids, remove_label_ids: ["INBOX", "UNREAD"],
                 add_label_ids: @add_labels

    @api.batch_modify_messages(@user, req)
  end

  def next_messages(last = nil)
    @api.list_user_messages(@user,
                            page_token: last,
                            label_ids: @cfg.fetch_labels,
                            max_results: @cfg.max_results,
                            q: @cfg.remote_query)
  end

  def send_message_str(body)
    send_message(StringIO.new(body))
  end

  def send_message(body)
    # body should be an io object
    msg = @api.send_user_message(
      @user,
      upload_source: body,
      content_type: 'message/rfc822'
    )
    log.info("Sent message #{msg.id}, #{msg.size_estimate}: #{msg.snippet}")
  end

  def log
    @cfg.log
  end

end
