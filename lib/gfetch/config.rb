require 'runcom'

class Config
  APP_NAME = 'gfetch'

  attr_reader :log

  @@config_keys = [
    "fetch_labels",
    "verbose",
    "fetched_labels",
    "maildir",
    "remote_user",
    "browser_command",
    "cred_file",
    "token_file",
    "max_results",
    "remote_query",
    "log_file",
    "add_labels"
  ]

  # set up each key present in the default configuration as a getter
  # which looks it's key up in the hash.
  @@config_keys.each do |p|
    self.send(:define_method, p) { get p }
  end

  def runtime_defaults
    { "cred_file" => @config.path.dirname }
  end

  def initialize
    defaults = {
      "fetch_labels" => ["INBOX"],
      "verbose" => false,
      "fetched_labels" => nil,
      "maildir" => "~/Maildir",
      "remote_user" => "me",
      "browser_command" => "xdg-open",
      "cred_file" => XDG::Config.new.home.join(APP_NAME, "credentials.json"),
      "token_file" => XDG::Cache.new.home.join("#{APP_NAME}_token_file"),
      "log_file" => XDG::Data.new.home.join("#{APP_NAME}_log"),
      "remote_query" => "is:unread",
      "add_labels" => []
    }

    @config =
      Runcom::Config.new "gfetch",
                         file_name: 'config.yml',
                         defaults: defaults

    @log = Logger.new(File.expand_path(log_file))

    if verbose
      if @config.path
        puts "Config read from: #{@config.path}"
      else
        puts "No config file found, using defaults. Searched:"
        @config.paths.each { |path| puts "\t#{path}" }
      end
    end

  end

  def get arg
    @config.to_h[arg.to_s]
  end
end
