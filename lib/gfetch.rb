require 'progress_bar'

require 'gfetch/config'
require 'gfetch/remote'
require 'gfetch/local'

class GFetch

  def initialize
    @cfg = Config.new
    @remote = Remote.new @cfg
    @local = Local.new @cfg
  end

  def log
    @cfg.log
  end

  def cmd_fetch
    log.info("Starting fetch")

    msgs = @remote.get_messages

    log.info("Messages to fetch: #{msgs.count}")

    bar = ProgressBar.new(msgs.count)

    msgs.each do |msg|
      @local.put_message msg
      bar.increment!
    end

    log.info("done")
  end

  def cmd_send
    log.info("Starting send")
    # read the message to send from standard input as a raw RFC822
    msg = ARGF.read
    @remote.send_message_str msg
  end

  def do_cmd(cmd)
    m = method(("cmd_" + cmd).to_sym)
    if m
      m.call
    else
      raise "Unknown command: #{cmd}"
    end
  end

  def self.main

    cmd = ARGV.shift || "fetch"

    fetcher = GFetch.new

    fetcher.do_cmd(cmd)

  end
end
